﻿<?php
echo '


<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Uzupełnij szkolenia</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>
<form enctype="multipart/form-data" method="post" action="file.php">
	<p>Wybierz swoją najładniejszą fotografię<br />Wygląd ma znaczenie... ;)</p>
	<input type="hidden" name="max_size" value="3145728">
	<input type="file" name="up_file" value=""><br />';
	if(isset($_FILES['up_file']))
	{


		if ((isset($_POST['cancel']))&&($_POST['cancel']=='Anuluj'))
		{
			header('Location: index.php');
			exit();
		}


	$file = $_FILES['up_file'];
	
	IF(isset($file['name']))
		{
			$patch = str_replace('file.php', '', $_SERVER['SCRIPT_FILENAME']);
			if(($file['type'] == 'image/png') || ($file['type'] == 'image/jpeg') || ($file['type'] == 'image/gif'))
			{
				if(($file['size'])<=($_POST['max_size']))
				{
					$x = getimagesize($file['tmp_name']);
					if(!is_array($x) or $x[0] < 2)
					{
						die('<p class="info">Zły plik graficzny</p>');
					}
					copy($file['tmp_name'], $patch.'//img//foto.jpg');
					header('location: index.php');
				}
				else
				{
					echo '<p class="info">Za duży plik</p>';
					exit();
				}
			}
			else
			{
				echo '<p class="info">Niewłaściwy format pliku.</p>';
			}
		}
	}
	else
	{
		echo '<p>Dopuszczalne formaty: jpeg, png, gif. Max rozmiar 3MB</p>';
	}
echo '<input type="submit" name="Wyślij" value="Wyślij">
	<input type="submit" name="cancel" value="Anuluj">
</form>
</body>
</html>';