﻿<?php
	session_start();

	require_once 'database.php';
	//error_reporting(E_ALL ^ E_NOTICE);
echo<<<head

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">

<head>
	<meta http-equiv="content-type" content="text/html; charset="utf-8" />
	<title>Adam Wlazło - CV</title>
	<meta name="keywords" content="Adam, Wlazło, CV" />
	<meta name="description" content="Adam Wlazło - 6 lat jako asystent projektanta, AutoCAD-designer, 7 miesięcy - tester oprogramowania, reszta mniej znacząca ale nie należy o niej nie wspomnieć w CV" />
	
	<link rel="icon" type="image/png" href="foto/favicon.png" width="16" height="16" />

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link href="css/default.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="jquery-3.2.1.min.js"></script>
	
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i" rel="stylesheet">

</head>

<body>

<div id="container">

head;

$user = $db->query('SELECT * FROM users');
$data = $user->fetch();

	if(isset($_SESSION['admin_logged']))
	{
		echo "<a href='logout.php' class='logout'>Wyloguj</a>";
	}
	echo '<div id="person" class="row section">';
		echo '<h1>DANE OSOBOWE</h1>';

		echo '<div class="col-lg-6 col-md-7 col-sm-8 col-xs-12">
		<h2>'.$data['name'].' '.$data['surname'].'</h2>
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata1">Adres:</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata2">'.$data['address'].'</div>
	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata1">Numer tel.:</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata2">'.$data['telephone'].'</div>
	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata1">E-mail:</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata2">'.$data['email'].'</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata1">Data urodzenia</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdata2">'.$data['birth_date'].'</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
		if(isset($_SESSION['admin_logged']))

		{
			echo '<a id="link" href="edit_personal.php?id_name=user_id&id='.$data['user_id'].'&table=users" class="edit">Edytuj</a>';
		}
		echo '</div>';
	echo '</div>';
		
	echo '<div class="col-lg-6 col-md-5 col-sm-4 col-xs-12">';
	
		if(isset($_SESSION['admin_logged']))
		{
			echo '<a href="file.php"><img class="img-responsive" src="img/foto.jpg"></a>';
		}
		else
		{
			echo '<img class="img-responsive" src="img/foto.jpg">';
		}

		echo '</div>';
	echo '</div>';

	$education = $db->query('SELECT * FROM education ORDER BY edu_id DESC');
	echo '<div id="edu" class="row section">';
		echo '<h1>EDUKACJA</h1>';
		
		foreach( $education->fetchAll() as $value )
		{
			echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subsec">';
				echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 date">'.$value['date_from_to'].'</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><h2>'.$value['school'].'</h2><p>'.$value['other_info'].'</p>';
				if(isset($_SESSION['admin_logged']))
				{
				echo '<a href="delete.php?id_name=edu_id&id='.$value['edu_id'].'&table=education" class="del">Usuń</a><a href="add_edu.php?id_name=edu_id&id='.$value['edu_id'].'&table=education" class="edit">Edytuj</a>';
				}
				echo '</div>';
			echo '</div>';
		}
		if(isset($_SESSION['admin_logged']))
		{
		echo '<div class="col-xs-12"><a href="add_edu.php?id_name=&table=" class="add">Dodaj</a></div>';
		}
	echo '</div>';

	$experience = $db->query('SELECT * FROM experience ORDER BY exp_id DESC');
	echo '<div id="exp" class="row section">';
		echo '<h1>DOŚWIADCZENIE</h1>';
		
		foreach( $experience->fetchAll() as $value )
		{
			echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subsec">';
				echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 date">'.$value['date_from_to'].'</div>';
				
				echo '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-left-padding"><h2>'.$value['position'].'</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-left-padding"><p>'.$value['workplace'].';</p></div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-left-padding"><p>'.$value['other_info'].'</p>';

					if(isset($_SESSION['admin_logged']))
					{
					echo '<a href="delete.php?id_name=exp_id&id='.$value['exp_id'].'&table=experience" class="del">Usuń</a><a href="add_exp.php?id_name=exp_id&id='.$value['exp_id'].'&table=experience" class="edit">Edytuj</a>';
					}
					
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}
		if(isset($_SESSION['admin_logged']))
		{
		echo '<div class="col-xs-12"><a href="add_exp.php?id_name=&table=" class="add">Dodaj</a></div>';
		}
	echo '</div>';

	$courses = $db->query('SELECT * FROM courses  ORDER BY courses_id DESC');
		
	echo '<div id="training" class="row section">';
		echo '<h1>SZKOLENIA I KURSY</h1>';

		foreach( $courses->fetchAll() as $value )
		{
			echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subsec">';
				echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 date">'.$value['date_from_to'].';</div>';
				echo '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><h2>'.$value['title'].'</h2><p>'.$value['other_info'].'</p>';
				if(isset($_SESSION['admin_logged']))
				{
				echo '<a href="delete.php?id_name=courses_id&id='.$value['courses_id'].'&table=courses" class="del">Usuń</a><a href="add_training.php?id_name=courses_id&id='.$value['courses_id'].'&table=courses" class="edit">Edytuj</a>';
				}
				
				echo '</div>';
			echo '</div>';
		}
		if(isset($_SESSION['admin_logged']))
		{
		echo '<div class="col-xs-12"><a href="add_training.php?id_name=&table=" class="add">Dodaj</a></div>';
		}
	echo '</div>';
	

	$skills = $db->query('SELECT * FROM skills');
		echo '<div id="skills" class="row section">';
			echo '<h1>UMIEJĘTNOŚCI</h1>';
			echo '<div class="col-xs-offset-0 col-xs-12 col-sm-offset-3 col-sm-8">
				<ul>';
				foreach( $skills->fetchAll() as $value )
				{
					//echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subsec">';
					echo '<li>'.$value['title'].'</li>';
					if(isset($_SESSION['admin_logged']))
					{
						echo '<a href="delete.php?id_name=skill_id&id='.$value['skill_id'].'&table=skills" class="del">Usuń</a><a href="add_skill.php?id_name=skill_id&id='.$value['skill_id'].'&table=skills" class="edit">Edytuj</a>';
					}
					//echo '</div>';
				}
				echo '</ul>';
			echo '</div>';
			if(isset($_SESSION['admin_logged']))
			{
			echo '<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12"><a href="add_skill.php?id_name=&table=" class="add">Dodaj</a></div>';
			}
			
			
		echo '</div>';
	
	$hobbies = $db->query('SELECT * FROM hobbies');
	$h_data = $hobbies->fetch();
	echo '<div id="hobbies" class="row section">';
		echo '<h1>ZAINTERESOWANIA</h1>';
		echo '<p>'.$h_data['title'].'</p>';

		if(isset($_SESSION['admin_logged']))
		{
		echo '<a href="delete.php?id_name=hobby_id&id='.$h_data['hobby_id'].'&table=hobbies" class="del">Usuń</a><a href="edit_hobbies.php?id_name=hobby_id&id='.$h_data['hobby_id'].'&table=hobbies" class="edit">Edytuj</a>';
		}
	echo '</div>';
	
	echo <<<foot
	
	<div id="clause" class="row section">
		<p>Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w mojej ofercie pracy dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z ustawą z dn. 29.08.97 o Ochronie Danych Osobowych DZ. Ust. z 2002r. Nr 101, poz. 926 z późn. zm.)</p>
	</div>

</div>

<div id="footer">
Copyright &copy; 2018 by <a href="https://www.linkedin.com/in/adamwlazlo/" target="_blank">Adam Wlazło</a>
</div>
	

foot;
// <div id="footer">
// 	<h6>Copyright &copy; 2017 by Adam Wlazło</h6><br />

// 	<!-- stat.4u.pl NiE KaSoWaC -->
// 	<a target=_top href="http://stat.4u.pl/?awlazlo2" title="statystyki stron WWW"><img alt="stat4u" src="http://adstat.4u.pl/s4u.gif" border="0"></a>
// 	<script language="JavaScript" type="text/javascript">
// 	<!--
// 	function s4upl() { return "&amp;r=er";}
// 	//-->
// 	</script>
// 	<script language="JavaScript" type="text/javascript" src="http://adstat.4u.pl/s.js?awlazlo2"></script>
// 	<script language="JavaScript" type="text/javascript">
// 	<!--
// 	s4uext=s4upl();
// 	document.write('<img alt="stat4u" src="http://stat.4u.pl/cgi-bin/s.cgi?i=awlazlo2'+s4uext+'" width="1" height="1">')
// 	//-->
// 	</script>
// 	<noscript><img alt="stat4u" src="http://stat.4u.pl/cgi-bin/s.cgi?i=awlazlo2&amp;r=ns" width="1" height="1"></noscript>
// 	<!-- stat.4u.pl KoNiEc -->

// 	<br /><br />

// </div>
echo <<<popup
<script>
function showPop(){
    //document.getElementById('popcontent').innerHTML="You may click the X in the lower right corner to close this window or you may click outside of this window to close it as well.";
    document.getElementById('popup').style.display="block";
    document.getElementById('backgr').style.display="block";
}//end iconPop JS function

function closePop(){
    document.getElementById('popup').style.display="none";
    document.getElementById('backgr').style.display="none";
}

function SprawdzFormularz(f) 
{
	if (f.imie.value=='')
	{	
		reminfo("Wpisz swoje imię.");
		return false;
	}
	if (f.email.value=='')
	{
		reminfo("Wpisz adres e-mail.");
		return false;
	}
	if ( ((f.email.value.indexOf('@',1))==-1)||(f.email.value.indexOf('.',1))==-1 )
	{
		reminfo("Wpisz poprawny e-mail.");
		return false;
	}
	else
	{
		return true;
	}
}

function addinfo()
{
	document.getElementById("forminfo").classList.add("info");
	document.getElementById("forminfo").classList.remove("info2");
}

function reminfo(statement)
{	
	document.getElementById("forminfo").classList.remove("info");
	document.getElementById("forminfo").classList.add("info2");
	document.getElementById("forminfo").innerHTML = statement;
}

</script>

<div onclick="closePop();" id="backgr"></div>

<div id="popcontent">

	<form target="_blank" action="http://www.implebot.pl/post.php" name="impleBOT.pl" method="post" onsubmit="return SprawdzFormularz(this)" id="popup">
		<span onclick="closePop();" title="Zamknij okno">&otimes;</span>
		<p id="popuptitle">Zostaw swoje dane</p><br /><br  >
		
		<input name="uid" type="hidden" value="59185">
		<input name="zrodlo" type="hidden" value="standard">
		
		<input name="imie" type="text" placeholder="Imię" onfocus="this.placeholder = ''" onblur="this.placeholder='Imię'"  />
		
		<input name="email" type="text"placeholder="E-mail" onfocus="this.placeholder = ''" onblur="this.placeholder='E-mail'" />
		<p id="forminfo" class="info"></p>
		<input type="submit" value="Wyślij" />
		<input type="reset" value="Wyczyść" onclick="addinfo()"; />
	</form>

</div>
<!--End pop up code when icon's clicked-->

    
<div id="panel">
 
    <div id="title" onmouseover="showPop();">
        ZOSTAW E-MAIL
    </div>
 
</div>
	
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</body>
</html>

popup;

?>