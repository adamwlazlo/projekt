﻿<?php
	
	session_start();

	if(!isset($_SESSION['admin_logged']))
	{
		header('Location: index.php');
		exit();
	}

	require_once 'database.php';
	
	if (isSet($_POST['title']))
	{
		if ((isset($_POST['cancel']))&&($_POST['cancel']=='Anuluj'))
		{
			header('Location: index.php');
			exit();
		}


		$id = isSet($_POST['id']) ? intval($_POST['id']) : 0;

		if ($id > 0)
		{
			$sth = $db->prepare('UPDATE `hobbies` SET `title`=:title,`user_id`=1 WHERE hobby_id = :id');
			$sth->bindParam(':id', $id);
		}
		else
		{
			$sth = $db->prepare('INSERT INTO `hobbies`(`hobby_id`, `title`, `user_id`) VALUES (NULL,:title,1)');
		}

		$sth->bindParam(':title', htmlentities($_POST['title'], ENT_QUOTES, "UTF-8"));
		$sth->execute();

		header('location: index.php');
	}


	$id_get = isSet($_GET['id']) ? intval($_GET['id']) : 0;
	
	$name = $_GET['id_name'];
	$table = $_GET['table'];

	if ($id_get > 0)
	{
		$sth = $db->prepare('SELECT * FROM '.$table.' WHERE '.$name.' = :id');
		$sth->bindParam(':id', $id_get);
		$sth->execute();

		$result = $sth->fetch();
		//print_r ($result['title']);
		//header('location: index.php');
	}
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Uzupełnij swoje hobby</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>
<form method="post" action="edit_hobbies.php">

	<?php
		if($id_get > 0)
		{
			echo '<input type="hidden" name="id" value="'.$id_get.'">';
		}
	?>
	<p>Napisz coś więcej o sobie, czym się interesujesz, jakie masz hobby, itp.</p>
	<textarea name="title"><?php
	if(isset($result['title']))
	{
		echo $result['title'];
	}
	?> </textarea><br/>
	<input type="submit" value="Zapisz">
	<input type="submit" name="cancel" value="Anuluj">

</form>

</body>
</html>