<?php

session_start();

require_once 'database.php';

// if (isset($_SESSION['admin_logged'])) //jeśli nie jestesmy zalogowani to sprawdzamy zmiena login z posta

// {
//     echo "zalogowany";
//     //header('location: index.php');
//     //exit();
// }
// else
// {
    if (isset($_POST['login'])) //jeśli istnieje zmienna login z formularza logowania
    {

  
        $login = filter_input(INPUT_POST, 'login'); //odczytanie zmiennych z formularza
        $password = filter_input(INPUT_POST, 'password'); // za pomocą funkcji filter_input
        //echo $login.' '.$password;
        
        $login_query = $db->prepare('SELECT user_id, password FROM users WHERE login = :login'); //zapytanie wyciagajace uzytkowniaka o podanym loginie
        $login_query->bindValue(':login', $login, PDO::PARAM_STR);
        $login_query->execute();

        //echo $login_query->rowCount();

        $admin = $login_query->fetch(); //wyciągnięty rekord zapisuje w tablicy asocjacyjnej

        //echo $admin['user_id']." ".$admin['password'];

        if($admin && password_verify($password, $admin['password'])) //sprawdzamy czy istnieje tablica $admin, czyli istnial login i wyciagniety zostal rekord, oraz dodatkowo sprawdzamy czy zaszyfrowane haso w bazie zgadza sie z tym podanym w formuarzu
        {
            //echo 'zalogowano';
            $_SESSION['admin_logged'] = $admin['user_id']; //jeśli logowanie się powiodło to w zmiennej sesyjnej zapisujemy id admina
            unset($_SESSION['login_error']); //i kasujemy zmienna sesjna z błedem logowania
            header('Location: index.php');
        }
        else
        {
            $_SESSION['login_error'] = '<p class="error">Nieprawidłowy login lub hasło</p>';
            header('Location: index.php'); //jeśli logowanie się nie udało to ustawiamy mienna sesyjna 'login_error' na true
            header('Location: admin/index.php'); //i nastepnie wychodzimy z tego pliku login.php i wracamy do formularza logowania 
            exit();
        }
    }
    else
    {
        header('location: admin/index.php'); //jeśli nie istnieje zmienna login czyli nie wypełniono formularza, to wracamy, wychodzimy z tego pliku i wracamy do formularza logowania 
        exit();
    }

//}
