﻿<?php
	
	session_start();
	if(!isset($_SESSION['admin_logged']))
	{
		header('Location: index.php');
		exit();
	}
	
	require_once 'database.php';

	if (isset($_POST['date_from_to']))
	{
		if ((isset($_POST['cancel']))&&($_POST['cancel']=='Anuluj'))
		{
			header('Location: index.php');
			exit();
		}

	$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
		if($id > 0)
		{
			$sth = $db->prepare('UPDATE `education` SET `date_from_to`=:date_from_to,`school`=:school,`other_info`=:other_info,`user_id`=1 WHERE edu_id = :id');
			$sth->bindParam(':id', $id);
		}
		else
		{
			$sth = $db->prepare('INSERT INTO `education`(`edu_id`, `date_from_to`, `school`, `other_info`, `user_id`) VALUES (NULL,:date_from_to,:school,:other_info,1)');
		}
				$sth->bindParam(':date_from_to', htmlentities($_POST['date_from_to'], ENT_QUOTES, "UTF-8"));
				$sth->bindParam(':school', htmlentities($_POST['school'], ENT_QUOTES, "UTF-8"));
				$sth->bindParam(':other_info', htmlentities($_POST['other_info'], ENT_QUOTES, "UTF-8"));
				$sth->execute();

		header('location: index.php');

	}
	

	$idget = isset($_GET['id']) ? intval($_GET['id']) : 0;
	$name = $_GET['id_name'];
	$table = $_GET['table'];

	
	if($idget > 0)
	{
		$sth = $db->prepare('SELECT * FROM '.$table.' WHERE '.$name.' = :id');
		$sth->bindParam(':id', $idget);
		$sth->execute();

		$result = $sth->fetch();
	}

?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Uzupełnij wykształcenie</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>

<form method="post" action="add_edu.php">
<?php
	if ($idget>0)
	{
		echo '<input type="hidden" name="id" value="'.$idget.'" />';
	}
?>

	<p>Opisz swoje wykształcenie:</p>
	Data: <input type="text" name="date_from_to" <?php
	if (isset($result['date_from_to']))
	{
		echo 'value="'.$result['date_from_to'].'"';
	}
	?> ><br/>
	Szkoła/Uczelnia: <input type="text" name="school" <?php
	if (isset($result['school']))
	{
		echo 'value="'.$result['school'].'"';
	}
	?> ><br/>
	Opis: <textarea name="other_info"><?php
	if (isset($result['other_info']))
	{
		echo $result['other_info'];
	}
	?> </textarea><br/>
	<input type="submit" value="Zapisz">
	<input type="submit" name="cancel" value="Anuluj">

</form>

</body>
</html>