﻿<?php
	session_start();

	if(!isset($_SESSION['admin_logged']))
	{
		header('Location: index.php');
		exit();
	}

	require_once 'database.php';
	$name = $_GET['id_name'];
	$id = isSet($_GET['id']) ? intval($_GET['id']) : 0;
	$table = $_GET['table'];

	if ((isset($_POST['delete']))&&($_POST['delete']=='Usuń'))
	{
		
		if($id > 0)
		{
			$sth = $db->prepare('DELETE FROM '.$table.' WHERE '.$name.' = :id');
			$sth->bindParam(':id', $id);
			$sth->execute();
			header('Location: index.php');
		}
		else
		{
			header('Location: index.php');
		}
	}
	else if ((isset($_POST['delete']))&&($_POST['delete']=='Anuluj'))
	{
		header('Location: index.php');
	}

?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Usuwanie wpisu</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>

<form action="" method="POST" >

	<p>Czy na pewno chcesz usunąć wpis?<br ?>Tej operacji nie można cofnąć.</p>
	<input type="submit" name="delete" value="Usuń">
	<input type="submit" name="delete" value="Anuluj">

</form>
</body>
</html>