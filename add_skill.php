﻿<?php
	
	session_start();

	if(!isset($_SESSION['admin_logged']))
	{
		header('Location: index.php');
		exit();
	}

	require_once 'database.php';
	
	if (isSet($_POST['skill']))
	{
		if ((isset($_POST['cancel']))&&($_POST['cancel']=='Anuluj'))
		{
			header('Location: index.php');
			exit();
		}


		$id = isSet($_POST['id']) ? intval($_POST['id']) : 0;

		if ($id > 0)
		{
			$sth = $db->prepare('UPDATE `skills` SET `title`=:skill,`user_id`=1 WHERE skill_id = :id');
			$sth->bindParam(':id', $id);
		}
		else
		{
			$sth = $db->prepare('INSERT INTO `skills`(`skill_id`, `title`, `user_id`) VALUES (NULL,:skill,1)');
		}

		$sth->bindParam(':skill', htmlentities($_POST['skill'], ENT_QUOTES, "UTF-8"));
		$sth->execute();

		header('location: index.php');
	}


	$id_get = isSet($_GET['id']) ? intval($_GET['id']) : 0;
	
	$name = $_GET['id_name'];
	$table = $_GET['table'];

	if ($id_get > 0)
	{
		$sth = $db->prepare('SELECT * FROM '.$table.' WHERE '.$name.' = :id');
		$sth->bindParam(':id', $id_get);
		$sth->execute();

		$result = $sth->fetch();
		//print_r ($result['title']);
		//header('location: index.php');
	}
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Uzupełnij swoje dane osobowe</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>
<form method="post" action="add_skill.php">

	<?php
		if($id_get > 0)
		{
			echo '<input type="hidden" name="id" value="'.$id_get.'">';
		}
	?>
	<p>Posiadasz jakąś ciekawą umiejętność?</p>
	Dodaj ją w tym miejscu: <input type="text" name="skill" <?php if(isset($result['title']))
	 {
		echo 'value=" '.$result['title'].'"'; 
	 }?>><br/>
	<input type="submit" value="Zapisz">
	<input type="submit" name="cancel" value="Anuluj">

</form>
</body>
</html>