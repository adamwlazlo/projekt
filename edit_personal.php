﻿<?php
	
	session_start();

	if(!isset($_SESSION['admin_logged']))
	{
		header('Location: index.php');
		exit();
	}

	require_once 'database.php';
	
	if (isSet($_POST['name']))
	{
		if ((isset($_POST['cancel']))&&($_POST['cancel']=='Anuluj'))
		{
			header('Location: index.php');
			exit();
		}


		$id = isSet($_POST['id']) ? intval($_POST['id']) : 0;

		if ($id > 0)
		{
			$sth = $db->prepare('UPDATE `users` SET `email`=:email,`name`=:name,`surname`=:surname,`birth_date`=:birth_date,`telephone`=:telephone,`address`=:address WHERE user_id = :id');
			$sth->bindParam(':id', $id);
		}
	

		$sth->bindParam(':email', htmlentities($_POST['email'], ENT_QUOTES, "UTF-8"));
		$sth->bindParam(':name', htmlentities($_POST['name'], ENT_QUOTES, "UTF-8"));
		$sth->bindParam(':surname', htmlentities($_POST['surname'], ENT_QUOTES, "UTF-8"));
		$sth->bindParam(':birth_date', htmlentities($_POST['birth_date'], ENT_QUOTES, "UTF-8"));
		$sth->bindParam(':telephone', htmlentities($_POST['telephone'], ENT_QUOTES, "UTF-8"));
		$sth->bindParam(':address', htmlentities($_POST['address'], ENT_QUOTES, "UTF-8"));
		$sth->execute();

		header('location: index.php');
	}

	$id_get = isSet($_GET['id']) ? intval($_GET['id']) : 0;
	
	$name = $_GET['id_name'];
	$table = $_GET['table'];

	if ($id_get > 0)
	{
		$sth = $db->prepare('SELECT * FROM '.$table.' WHERE '.$name.' = :id');
		$sth->bindParam(':id', $id_get);
		$sth->execute();

		$result = $sth->fetch();
		//print_r ($result['title']);
		//header('location: index.php');
	}
	
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Uzupełnij swoje dane osobowe</title>
	
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	
</head>

<body>
<form method="post" action="edit_personal.php" class="form">

<?php
	if ($id_get>0)
	{
		echo '<input type="hidden" name="id" value="'.$id_get.'" />';
	}
?>
	<p>Wypełnij tutaj swoje podstawowe dane osobowe</p>
	Imię: <input type="text" name="name" <?php
	if (isset($result['name']))
	{
		echo 'value="'.$result['name'].'"';
	}
	?> ><br/>
	Nazwisko: <input type="text" name="surname" <?php
	if (isset($result['surname']))
	{
		echo 'value="'.$result['surname'].'"';
	}
	?> ><br/>
	Adres: <input type="text" name="address" <?php
	if (isset($result['address']))
	{
		echo 'value="'.$result['address'].'"';
	}
	?> ><br/>
	Nr tel.: <input type="text" name="telephone" <?php
	if (isset($result['telephone']))
	{
		echo 'value="'.$result['telephone'].'"';
	}
	?> ><br/>
	Email: <input type="text" name="email" <?php
	if (isset($result['email']))
	{
		echo 'value="'.$result['email'].'"';
	}
	?> ><br/>
	Data urodzenia: <input type="date" name="birth_date" <?php
	if (isset($result['birth_date']))
	{
		echo 'value="'.$result['birth_date'].'"';
	}
	?> ><br/>
	<input type="submit" value="Zapisz">
	<input type="submit" name="cancel" value="Anuluj">

</form>
</body>
</html>